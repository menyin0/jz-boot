//package org.jeecg.modules;
//
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.task.TaskExecutor;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//
///**
// * @Description: TODO
// * @author: geduo
// * @date: 2024年06月17日 14:24
// */
//@SpringBootApplication(exclude = DynamicDataSourceAutoConfiguration.class)
//@EnableCaching
//@EnableAsync
//public class BuyerApplication {
//
//    @Primary
//    @Bean
//    public TaskExecutor primaryTaskExecutor() {
//        return new ThreadPoolTaskExecutor();
//    }
//
//    public static void main(String[] args) {
//        System.setProperty("es.set.netty.runtime.available.processors", "false");
//        System.setProperty("rocketmq.client.logUseSlf4j","true");
//        ConfigurableApplicationContext ac= SpringApplication.run(BuyerApplication.class, args);
//        for (int i = 0; i < ac.getBeanDefinitionNames().length; i++) {
//            System.out.println(ac.getBeanDefinitionNames()[i]);
//        }
//    }
//}
