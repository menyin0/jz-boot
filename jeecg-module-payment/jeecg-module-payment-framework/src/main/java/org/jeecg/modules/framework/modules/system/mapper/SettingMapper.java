package org.jeecg.modules.framework.modules.system.mapper;

import org.jeecg.modules.framework.modules.system.entity.dos.Setting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 配置数据处理层
 * @author Chopper
 * @since 2020/11/17 8:01 下午
 */
public interface SettingMapper extends BaseMapper<Setting> {

}
