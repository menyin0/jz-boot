package org.jeecg.modules.framework.modules.system.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.framework.modules.system.entity.dos.Setting;
import org.jeecg.modules.framework.modules.system.mapper.SettingMapper;
import org.jeecg.modules.framework.modules.system.service.SettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 配置业务层实现
 *
 * @author Chopper
 * @since 2020/11/17 3:52 下午
 */
@Service
public class SettingServiceImpl extends ServiceImpl<SettingMapper, Setting> implements SettingService {

    @Override
    public Setting get(String key) {
        LambdaQueryWrapper<Setting> lambdaQueryWrapper= new LambdaQueryWrapper<Setting>();
        lambdaQueryWrapper.eq(Setting::getSettingKey,key);
        return this.getOne(lambdaQueryWrapper);
//        return this.getById(key);//换成settingKey来查询
    }

    @Override
    public boolean saveUpdate(Setting setting) {
        return this.saveOrUpdate(setting);
    }
}
