package org.jeecg.modules.framework.modules.payment.kit.params.impl;

import cn.hutool.json.JSONUtil;
import org.jeecg.modules.framework.modules.payment.entity.enums.CashierEnum;
import org.jeecg.modules.framework.modules.payment.kit.dto.PayParam;
import org.jeecg.modules.framework.modules.payment.kit.dto.PaymentSuccessParams;
import org.jeecg.modules.framework.modules.payment.kit.params.CashierExecute;
import org.jeecg.modules.framework.modules.payment.kit.params.dto.CashierParam;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description: TODO
 * @author: geduo
 * @date: 2024年06月21日 15:17
 */
@Component
public class ContractCashier implements CashierExecute {

    /***
     * 交易类型为Contract时，调用下单接口前/时将订单的支付信息、支持支付方式、合同设置信息封装到CashierParam中
     * @param payParam 收银台支付参数
     * @return
     */
    @Override
    public CashierParam getPaymentParams(PayParam payParam) {
       /* if (payParam.getOrderType().equals(CashierEnum.CONTRACT.name())) {
            //准备返回的数据
            CashierParam cashierParam = new CashierParam();
            //订单信息获取
            OrderDetailVO order = orderService.queryDetail(payParam.getSn());

            //如果订单已支付，则不能发器支付
            if (order.getOrder().getPayStatus().equals(PayStatusEnum.PAID.name())) {
                throw new ServiceException(ResultCode.PAY_DOUBLE_ERROR);
            }
            //如果订单状态不是待付款，则抛出异常
            if (!order.getOrder().getOrderStatus().equals(OrderStatusEnum.UNPAID.name())) {
                throw new ServiceException(ResultCode.PAY_BAN);
            }
            cashierParam.setPrice(order.getOrder().getFlowPrice());

            try {
                BaseSetting baseSetting = JSONUtil.toBean(settingService.get(SettingEnum.BASE_SETTING.name()).getSettingValue(), BaseSetting.class);
                cashierParam.setTitle(baseSetting.getSiteName());
            } catch (Exception e) {
                cashierParam.setTitle("多用户商城，在线支付");
            }


            List<OrderItem> orderItemList = order.getOrderItems();
            StringBuilder subject = new StringBuilder();
            for (OrderItem orderItem : orderItemList) {
                subject.append(orderItem.getGoodsName()).append(";");
            }

            cashierParam.setDetail(subject.toString());

            cashierParam.setOrderSns(payParam.getSn());
            cashierParam.setCreateTime(order.getOrder().getCreateTime());
            return cashierParam;
        }*/

        return null;
    }

    /***
     * 交易类型为Contract时，支付成功回调后的处理
     * @param paymentSuccessParams 支付回调
     */
    @Override
    public void paymentSuccess(PaymentSuccessParams paymentSuccessParams) {

    }

    /***
     * 交易类型为Contract时，查询支付结果信息
     * @param payParam
     * @return
     */
    @Override
    public Boolean paymentResult(PayParam payParam) {
        return null;
    }

    @Override
    public CashierEnum cashierEnum() {
        return CashierEnum.CONTRACT;
    }
}
