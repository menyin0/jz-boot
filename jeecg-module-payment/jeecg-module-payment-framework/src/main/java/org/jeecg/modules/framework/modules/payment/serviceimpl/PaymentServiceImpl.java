package org.jeecg.modules.framework.modules.payment.serviceimpl;

import org.jeecg.modules.framework.modules.payment.kit.CashierSupport;
import org.jeecg.modules.framework.modules.payment.kit.dto.PaymentSuccessParams;
import org.jeecg.modules.framework.modules.payment.kit.params.CashierExecute;
import org.jeecg.modules.framework.modules.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private List<CashierExecute> cashierExecutes;
    @Autowired
    private CashierSupport cashierSupport;

    @Override
    public void success(PaymentSuccessParams paymentSuccessParams) {

        //支付状态 cny:订单表里有记录则返回
        boolean paymentResult = cashierSupport.paymentResult(paymentSuccessParams.getPayParam());

        //已支付则返回
        if (paymentResult) {
            log.warn("收银台重复收款，流水号：{}", paymentSuccessParams.getReceivableNo());
            return;
        }

        log.debug("支付成功，第三方流水：{}", paymentSuccessParams.getReceivableNo());
        //支付结果处理  cny:订单表里没有记录则保存记录，这里会根据交易的类型插入不同的表，即CashierExecute有不同实现
        for (CashierExecute cashierExecute : cashierExecutes) {
            cashierExecute.paymentSuccess(paymentSuccessParams);
        }
    }

    @Override
    public void adminPaySuccess(PaymentSuccessParams paymentSuccessParams) {

        log.debug("支付状态修改成功->银行转账");
        //支付结果处理
        for (CashierExecute cashierExecute : cashierExecutes) {
            cashierExecute.paymentSuccess(paymentSuccessParams);
        }
    }
}
