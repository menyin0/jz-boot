package org.jeecg.modules.framework.modules.system.entity.dos;

import org.jeecg.common.system.base.entity.JeecgEntity;
//import org.jeecg.modules.framework.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 设置
 * @author Chopper
 * @since 2020-02-25 14:10:16
 */
@Data
@TableName("pay_setting")
@ApiModel(value = "支付配置")
@NoArgsConstructor
public class Setting extends JeecgEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "配置值value")
    private String settingValue;
    @ApiModelProperty(value = "配置key")
    private String settingKey;//cny_add

}
