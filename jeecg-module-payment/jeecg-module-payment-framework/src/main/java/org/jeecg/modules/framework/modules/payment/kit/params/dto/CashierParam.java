package org.jeecg.modules.framework.modules.payment.kit.params.dto;

import cn.hutool.core.text.CharSequenceUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 支付参数
 * cny:支付详情，用于返回给用户的
 *
 * @author Chopper
 * @since 2021-01-25 19:09
 */
@Data
@ToString
public class CashierParam {

    static final Long MAX_DETAIL_LENGTH = 30L;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "支付title")
    private String title;

    @ApiModelProperty(value = "支付详细描述")
    private String detail;

    /**
     * 当交易类型为多单合并的Trade类型时填入子订单编号
     */
    @ApiModelProperty(value = "订单sn集合")
    private String orderSns;
    /**
     * 当下单后调用下单接口前，前端用于获取支付方式列表显示给用户选择
     */
    @ApiModelProperty(value = "支持支付方式")
    private List<String> support;


    @ApiModelProperty(value = "订单创建时间")
    private Date createTime;

    @ApiModelProperty(value = "支付自动结束时间")
    private Long autoCancel;

    @ApiModelProperty(value = "剩余余额") // cny_note:订单中附加账户余额，供买家查看知悉，实际没有太大用处
    private Double walletValue;

    public String getDetail() {
        if (CharSequenceUtil.isEmpty(detail)) {
            return "清单详细";
        }
        return filterSpecialChart(sub(detail, 30));
    }

    /**
     * 过滤特殊字符串
     *
     * @param str
     * @return
     */
    public static String filterSpecialChart(String str) {
        String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }
    /**
     * 切割字符串
     *
     * @param str    字符串
     * @param length 长度
     * @return 处理后的字符串
     */
    public static String sub(String str, Integer length) {
        if (str.length() < length) {
            return str;
        }
        return str.substring(0, length);
    }
}
