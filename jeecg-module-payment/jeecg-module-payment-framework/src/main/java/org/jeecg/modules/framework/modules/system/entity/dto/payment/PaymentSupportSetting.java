package org.jeecg.modules.framework.modules.system.entity.dto.payment;

import org.jeecg.modules.framework.common.enums.ClientTypeEnum;
import org.jeecg.modules.framework.modules.payment.entity.enums.PaymentMethodEnum;
import org.jeecg.modules.framework.modules.system.entity.dto.payment.dto.PaymentSupportForm;
import org.jeecg.modules.framework.modules.system.entity.dto.payment.dto.PaymentSupportItem;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 支持的支付方式
 *
 * @author Chopper
 * @since 2021-01-26 15:52
 */
@Data
@Accessors(chain = true)
public class PaymentSupportSetting {

    private List<PaymentSupportItem> paymentSupportItems;


    public PaymentSupportSetting() {

    }

    public PaymentSupportSetting(PaymentSupportForm paymentSupportForm) {

        List<PaymentSupportItem> paymentSupportItems = new ArrayList<>();

        for (ClientTypeEnum client : paymentSupportForm.getClients()) {
            PaymentSupportItem paymentSupportItem = new PaymentSupportItem();

            List<String> supports = new ArrayList<>();
            for (PaymentMethodEnum payment : paymentSupportForm.getPayments()) {
                supports.add(payment.name());
            }
            paymentSupportItem.setClient(client.name());
            paymentSupportItem.setSupports(supports);
            paymentSupportItems.add(paymentSupportItem);

        }
        this.paymentSupportItems = paymentSupportItems;
    }
}
