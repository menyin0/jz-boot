package org.jeecg.modules.framework.common.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 雪花分布式id获取
 *
 * @author Chopper
 */
@Slf4j
public class SnowFlake {

//    /**
//     * 机器id
//     */
//    private static long workerId = 0L;
//    /**
//     * 机房id
//     */
//    public static long datacenterId = 0L;

    private static Snowflake snowflake;

    /**
     * 初始化配置
     *
     * @param workerId
     * @param datacenterId
     */
    public static void initialize(long workerId, long datacenterId) {
        snowflake = IdUtil.getSnowflake(workerId, datacenterId);
    }

    public static long getId() {
        return snowflake.nextId();
    }

    /**
     * 生成字符，带有前缀
     *
     * @param prefix
     * @return
     */
    public static String createStr(String prefix) {
        return prefix + dateToString(new Date(), "yyyyMMdd") + SnowFlake.getId();
    }

    public static String getIdStr() {
        return snowflake.nextId() + "";
    }



    /**
     * cny add
     * 把日期转换成字符串型
     *
     * @param date    日期
     * @param pattern 类型
     * @return 字符串时间
     */
    public static String dateToString(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        if (pattern == null) {
            pattern = "yyyy-MM-dd";
        }
        String dateString = "";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            dateString = sdf.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dateString;
    }
}
