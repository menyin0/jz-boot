package org.jeecg.modules.demo.jz.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.jz.entity.JzResume;
import org.jeecg.modules.demo.jz.service.IJzResumeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 简历信息表
 * @Author: jeecg-boot
 * @Date:   2024-07-04
 * @Version: V1.0
 */
@Api(tags="简历信息表")
@RestController
@RequestMapping("/jz/jzResume")
@Slf4j
public class JzResumeController extends JeecgController<JzResume, IJzResumeService> {
	@Autowired
	private IJzResumeService jzResumeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param jzResume
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "简历信息表-分页列表查询")
	@ApiOperation(value="简历信息表-分页列表查询", notes="简历信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<JzResume>> queryPageList(JzResume jzResume,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<JzResume> queryWrapper = QueryGenerator.initQueryWrapper(jzResume, req.getParameterMap());
		Page<JzResume> page = new Page<JzResume>(pageNo, pageSize);
		IPage<JzResume> pageList = jzResumeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param jzResume
	 * @return
	 */
	@AutoLog(value = "简历信息表-添加")
	@ApiOperation(value="简历信息表-添加", notes="简历信息表-添加")
	@RequiresPermissions("jz:jz_resume:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody JzResume jzResume) {
		jzResumeService.save(jzResume);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param jzResume
	 * @return
	 */
	@AutoLog(value = "简历信息表-编辑")
	@ApiOperation(value="简历信息表-编辑", notes="简历信息表-编辑")
	@RequiresPermissions("jz:jz_resume:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody JzResume jzResume) {
		jzResumeService.updateById(jzResume);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "简历信息表-通过id删除")
	@ApiOperation(value="简历信息表-通过id删除", notes="简历信息表-通过id删除")
	@RequiresPermissions("jz:jz_resume:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		jzResumeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "简历信息表-批量删除")
	@ApiOperation(value="简历信息表-批量删除", notes="简历信息表-批量删除")
	@RequiresPermissions("jz:jz_resume:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.jzResumeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "简历信息表-通过id查询")
	@ApiOperation(value="简历信息表-通过id查询", notes="简历信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<JzResume> queryById(@RequestParam(name="id",required=true) String id) {
		JzResume jzResume = jzResumeService.getById(id);
		if(jzResume==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(jzResume);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param jzResume
    */
    @RequiresPermissions("jz:jz_resume:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, JzResume jzResume) {
        return super.exportXls(request, jzResume, JzResume.class, "简历信息表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("jz:jz_resume:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, JzResume.class);
    }

}
