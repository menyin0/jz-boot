package org.jeecg.modules.demo.jz.service.impl;

import org.jeecg.modules.demo.jz.entity.JzResume;
import org.jeecg.modules.demo.jz.mapper.JzResumeMapper;
import org.jeecg.modules.demo.jz.service.IJzResumeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 简历信息表
 * @Author: jeecg-boot
 * @Date:   2024-07-04
 * @Version: V1.0
 */
@Service
public class JzResumeServiceImpl extends ServiceImpl<JzResumeMapper, JzResume> implements IJzResumeService {

}
