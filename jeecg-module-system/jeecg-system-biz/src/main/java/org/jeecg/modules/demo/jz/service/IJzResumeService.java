package org.jeecg.modules.demo.jz.service;

import org.jeecg.modules.demo.jz.entity.JzResume;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 简历信息表
 * @Author: jeecg-boot
 * @Date:   2024-07-04
 * @Version: V1.0
 */
public interface IJzResumeService extends IService<JzResume> {

}
