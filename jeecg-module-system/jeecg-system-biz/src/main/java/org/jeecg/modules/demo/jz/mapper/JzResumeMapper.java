package org.jeecg.modules.demo.jz.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.jz.entity.JzResume;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 简历信息表
 * @Author: jeecg-boot
 * @Date:   2024-07-04
 * @Version: V1.0
 */
public interface JzResumeMapper extends BaseMapper<JzResume> {

}
