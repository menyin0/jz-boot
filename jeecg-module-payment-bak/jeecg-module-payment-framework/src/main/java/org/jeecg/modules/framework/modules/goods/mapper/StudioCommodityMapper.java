package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 直播间-商品关联持久层
 * @author Bulbasaur
 * @since 2021/5/17 3:14 下午
 *
 */
public interface StudioCommodityMapper extends BaseMapper<StudioCommodity> {
}
