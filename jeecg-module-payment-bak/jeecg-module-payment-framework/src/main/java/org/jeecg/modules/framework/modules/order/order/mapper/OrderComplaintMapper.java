package org.jeecg.modules.framework.modules.order.order.mapper;

import org.jeecg.modules.framework.modules.order.order.entity.dos.OrderComplaint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单投诉数据处理层
 *
 * @author paulG
 * @since 2020/12/5
 **/
public interface OrderComplaintMapper extends BaseMapper<OrderComplaint> {
}
