package org.jeecg.modules.framework.modules.order.aftersale.mapper;

import org.jeecg.modules.framework.modules.order.aftersale.entity.dos.AfterSaleLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 售后日志数据处理层
 *
 * @author Bulbasaur
 * @since 2020-02-25 14:10:16
 */
public interface AfterSaleLogMapper extends BaseMapper<AfterSaleLog> {

}
