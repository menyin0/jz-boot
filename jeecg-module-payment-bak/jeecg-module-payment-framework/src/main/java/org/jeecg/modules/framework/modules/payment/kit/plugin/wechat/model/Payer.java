package org.jeecg.modules.framework.modules.payment.kit.plugin.wechat.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一下单-支付者
 *
 * @author Chopper
 * @since 2020/12/17 17:56
 */

@Data
@Accessors(chain = true)
public class Payer {
    /**
     * 用户标识
     */
    private String openid;//商户时传递此字段，服务商传递以下两个字段
    /**
     * 用户服务标识
     */
    private String sp_openid;
    /**
     * 用户子标识
     */
    private String sub_openid;
}
