package org.jeecg.modules.framework.mybatis;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 数据库基础实体类
 *
 * @author Chopper
 * @version v1.0
 * @since 2020/8/20 14:34
 */
@Data
@JsonIgnoreProperties(value = { "handler", "fieldHandler"})
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseEntity implements Serializable {//TODO cny 后续将BaseEntity替换为JeecgEntity

    private static final long serialVersionUID = 1L;
/*
createTime	创建日期
createBy	创建人用户账号
updateTime	修改日期
updateBy	更新人用户账号
sysOrgCode
*/

    //@TableId
    @TableId(type = IdType.ASSIGN_ID)//转由jeecg控制
    @ApiModelProperty(value = "主键", hidden = true)
    private String id;


    //@CreatedBy //作用：Data MongoDB、Spring Data Elasticsearch等模块一起使用，用于标识一个字段作为实体的创建者，暂时删除
    //@TableField(fill = FieldFill.INSERT)//转由jeecg控制
    @ApiModelProperty(value = "创建人", hidden = true)
    private String createBy;

    //@CreatedDate //作用：Data MongoDB、Spring Data Elasticsearch等模块一起使用，用于标识一个字段作为实体的创建时间，暂时删除
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@TableField(fill = FieldFill.INSERT)//转由jeecg控制
    @ApiModelProperty(value = "创建日期", hidden = true)
    //@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis") //ES相关暂时不要
    private Date createTime;


    //@LastModifiedBy //作用：Data MongoDB、Spring Data Elasticsearch等模块一起使用，用于标识一个字段作为实体的更新人，暂时删除
    //@TableField(fill = FieldFill.UPDATE)//转由jeecg控制
    @ApiModelProperty(value = "更新人", hidden = true)
    private String updateBy;

    //@LastModifiedDate //作用：Data MongoDB、Spring Data Elasticsearch等模块一起使用，用于标识一个字段作为实体的更新时间，暂时删除
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@TableField(fill = FieldFill.INSERT_UPDATE) //转由jeecg控制
    @ApiModelProperty(value = "更新日期", hidden = true)
    //@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis") //ES相关暂时不要
    private Date updateTime;

    /*@TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "删除标志 true/false 删除/未删除", hidden = true)
    private Boolean deleteFlag;*/

}
