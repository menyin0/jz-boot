package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品参数数据处理层
 *
 * @author pikachu
 * @since 2020-03-02 17:27:56
 */
public interface ParametersMapper extends BaseMapper<Parameters> {


}
