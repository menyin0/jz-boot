package org.jeecg.modules.framework.modules.order.aftersale.mapper;

import org.jeecg.modules.framework.modules.order.aftersale.entity.dos.AfterSaleReason;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 售后原因数据处理层
 *
 * @author Chopper
 * @since 2020/11/17 7:34 下午
 */
public interface AfterSaleReasonMapper extends BaseMapper<AfterSaleReason> {


}
