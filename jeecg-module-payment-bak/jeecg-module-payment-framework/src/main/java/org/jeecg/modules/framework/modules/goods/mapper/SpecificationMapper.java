package org.jeecg.modules.framework.modules.goods.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 规格数据处理层
 *
 * @author pikachu
 * @since 2020-02-18 15:18:56
 */
public interface SpecificationMapper extends BaseMapper<Specification> {

}
