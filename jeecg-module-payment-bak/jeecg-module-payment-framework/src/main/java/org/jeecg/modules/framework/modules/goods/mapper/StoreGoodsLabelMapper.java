package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 店铺商品分类数据处理层
 *
 * @author pikachu
 * @since 2020-03-07 09:18:56
 */
public interface StoreGoodsLabelMapper extends BaseMapper<StoreGoodsLabel> {
}
