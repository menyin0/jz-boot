package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品关键字数据处理层
 *
 * @author paulG
 * @since 2020/10/15
 **/
public interface GoodsWordsMapper extends BaseMapper<GoodsWords> {

}
