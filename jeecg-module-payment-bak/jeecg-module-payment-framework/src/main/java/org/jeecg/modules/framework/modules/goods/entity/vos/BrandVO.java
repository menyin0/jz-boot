package org.jeecg.modules.framework.modules.goods.entity.vos;

import lombok.Data;

/**
 * 品牌VO
 *
 * @author pikachu
 * @since 2020-02-26 23:24:13
 */
@Data
public class BrandVO extends Brand {

    private static final long serialVersionUID = 3829199991161122317L;

}
