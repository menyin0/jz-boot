package org.jeecg.modules.framework.modules.connect.entity.dto;

import org.jeecg.modules.framework.modules.member.entity.dos.Member;
import lombok.Data;

/**
 * 会员联合登录消息
 */
@Data
public class MemberConnectLoginMessage {

    private Member member;
    private ConnectAuthUser connectAuthUser;
}
