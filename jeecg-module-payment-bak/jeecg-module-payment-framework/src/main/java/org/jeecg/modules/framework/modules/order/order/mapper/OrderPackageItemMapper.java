package org.jeecg.modules.framework.modules.order.order.mapper;

import org.jeecg.modules.framework.modules.order.order.entity.dos.OrderPackageItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 子订单包裹数据处理层
 *
 * @author Bulbasaur
 * @since 2020/11/17 7:34 下午
 */
public interface OrderPackageItemMapper extends BaseMapper<OrderPackageItem> {

}
