package org.jeecg.modules.framework.modules.member.entity.vo;

import org.jeecg.modules.framework.common.security.token.Token;
import lombok.Data;

@Data
public class QRLoginResultVo {

    private Token token;

    private int status;
}
