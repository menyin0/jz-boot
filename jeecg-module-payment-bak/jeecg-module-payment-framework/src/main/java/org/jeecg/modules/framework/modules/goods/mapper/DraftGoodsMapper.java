package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 草稿商品数据处理层
 *
 * @author paulG
 * @since 2020/12/19
 **/
public interface DraftGoodsMapper extends BaseMapper<DraftGoods> {
}
