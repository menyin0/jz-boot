package org.jeecg.modules.framework.modules.goods.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品计量单位数据处理层
 *
 * @author Bulbasaur
 * @since 2020/11/26 16:11
 */
public interface GoodsUnitMapper extends BaseMapper<GoodsUnit> {


}
