package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author paulG
 * @since 2022/5/24
 **/
public interface WholesaleMapper extends BaseMapper<Wholesale> {
}
