package org.jeecg.modules.framework.modules.connect.mapper;

import org.jeecg.modules.framework.modules.connect.entity.Connect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 联合登陆数据处理层
 *
 * @author Chopper
 */
public interface ConnectMapper extends BaseMapper<Connect> {

}
