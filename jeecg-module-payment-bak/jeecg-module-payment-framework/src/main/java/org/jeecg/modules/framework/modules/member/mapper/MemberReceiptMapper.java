package org.jeecg.modules.framework.modules.member.mapper;

import org.jeecg.modules.framework.modules.member.entity.dos.MemberReceipt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * 会员发票数据层
 *
 * @author Chopper
 * @since 2021-03-29 14:10:16
 */
public interface MemberReceiptMapper extends BaseMapper<MemberReceipt> {

}
