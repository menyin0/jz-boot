package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 直播间数据层
 *
 * @author Bulbasaur
 * @since 2021/5/17 9:56 上午
 */
public interface StudioMapper extends BaseMapper<Studio> {

}
