package org.jeecg.modules.framework.modules.sms.plugin;

import cn.hutool.json.JSONUtil;
import org.jeecg.modules.framework.common.exception.ServiceException;
import org.jeecg.modules.framework.modules.sms.entity.enums.SmsEnum;
import org.jeecg.modules.framework.modules.sms.plugin.impl.AliSmsPlugin;
import org.jeecg.modules.framework.modules.sms.plugin.impl.HuaweiSmsPlugin;
import org.jeecg.modules.framework.modules.sms.plugin.impl.TencentSmsPlugin;
import org.jeecg.modules.framework.modules.system.entity.dos.Setting;
import org.jeecg.modules.framework.modules.system.entity.dto.SmsSetting;
import org.jeecg.modules.framework.modules.system.entity.enums.SettingEnum;
import org.jeecg.modules.framework.modules.system.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 短信服务抽象工厂 直接返回操作类
 *
 * @author Bulbasaur
 * @version v1.0
 * @since 2023-02-16
 */
@Component
public class SmsPluginFactory {

    @Autowired
    private SettingService settingService;


    /**
     * 获取oss client
     *
     * @return
     */
    public SmsPlugin smsPlugin() {

        SmsSetting smsSetting = null;
        try {
            Setting setting = settingService.get(SettingEnum.SMS_SETTING.name());

            smsSetting = JSONUtil.toBean(setting.getSettingValue(), SmsSetting.class);


            switch (SmsEnum.valueOf(smsSetting.getType())) {

                case ALI:
                    return new AliSmsPlugin(smsSetting);
                case TENCENT:
                    return new TencentSmsPlugin(smsSetting);
                case HUAWEI:
                    return new HuaweiSmsPlugin(smsSetting);
                default:
                    throw new ServiceException();
            }
        } catch (Exception e) {
            throw new ServiceException();
        }
    }
}
