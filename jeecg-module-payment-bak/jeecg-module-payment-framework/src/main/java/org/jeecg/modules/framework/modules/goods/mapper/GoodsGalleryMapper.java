package org.jeecg.modules.framework.modules.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * 商品相册数据处理层
 *
 * @author pikachu
 * @since 2020-02-18 15:18:56
 */
public interface GoodsGalleryMapper extends BaseMapper<GoodsGallery> {



}
