package org.jeecg.modules.framework.modules.payment.kit.params.dto;

import cn.hutool.core.text.CharSequenceUtil;
import org.jeecg.modules.framework.common.utils.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * 支付参数  cny:支付详情，用于返回给用户
 *
 * @author Chopper
 * @since 2021-01-25 19:09
 */
@Data
@ToString
public class CashierParam {

    static final Long MAX_DETAIL_LENGTH = 30L;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "支付title")
    private String title;

    @ApiModelProperty(value = "支付详细描述")
    private String detail;

    /**
     * 当交易类型为多单合并的Trade类型时填入子订单编号
     */
    @ApiModelProperty(value = "订单sn集合")
    private String orderSns;
    /**
     * 当下单后调用下单接口前，前端用于获取支付方式列表显示给用户选择
     */
    @ApiModelProperty(value = "支持支付方式")
    private List<String> support;


    @ApiModelProperty(value = "订单创建时间")
    private Date createTime;

    @ApiModelProperty(value = "支付自动结束时间")
    private Long autoCancel;

    @ApiModelProperty(value = "剩余余额") // cny_note:订单中附加账户余额，供买家查看知悉，实际没有太大用处
    private Double walletValue;

    public String getDetail() {
        if (CharSequenceUtil.isEmpty(detail)) {
            return "清单详细";
        }
        return StringUtils.filterSpecialChart(StringUtils.sub(detail, 30));
    }
}
