package org.jeecg.modules.buyer.controller.wallet;

import org.jeecg.modules.framework.common.enums.ResultUtil;
import org.jeecg.modules.framework.common.security.context.UserContext;
import org.jeecg.modules.framework.common.vo.PageVO;
import org.jeecg.modules.framework.common.vo.ResultMessage;
import org.jeecg.modules.framework.modules.order.trade.entity.vo.RechargeQueryVO;
import org.jeecg.modules.framework.modules.wallet.entity.dos.Recharge;
import org.jeecg.modules.framework.modules.wallet.service.RechargeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 买家端,预存款充值记录接口
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "买家端,预存款充值记录接口")
@RequestMapping("/buyer/wallet/recharge")
public class RechargeBuyerController {

    @Autowired
    private RechargeService rechargeService;

    @ApiOperation(value = "分页获取预存款充值记录")
    @GetMapping
    public ResultMessage<IPage<Recharge>> getByPage(PageVO page) {
        //构建查询参数
        RechargeQueryVO rechargeQueryVO = new RechargeQueryVO();
        rechargeQueryVO.setMemberId(UserContext.getCurrentUser().getId());
        //构建查询 返回数据
        IPage<Recharge> rechargePage = rechargeService.rechargePage(page, rechargeQueryVO);
        return ResultUtil.data(rechargePage);
    }
}
